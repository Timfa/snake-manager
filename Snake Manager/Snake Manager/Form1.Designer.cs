﻿
namespace Snake_Manager
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.financeTab = new System.Windows.Forms.TabPage();
            this.snakesTab = new System.Windows.Forms.TabPage();
            this.selectedSnakeName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.snakeSelector = new System.Windows.Forms.ComboBox();
            this.newSnakeButton = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fatherSelector = new System.Windows.Forms.ComboBox();
            this.motherSelector = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.genderBox = new System.Windows.Forms.ComboBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.speciesBox = new System.Windows.Forms.ComboBox();
            this.newSpecies = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.NewGene = new System.Windows.Forms.Button();
            this.geneBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.deleteSpecies = new System.Windows.Forms.Button();
            this.speciesName = new System.Windows.Forms.TextBox();
            this.speciesDescription = new System.Windows.Forms.TextBox();
            this.geneDescription = new System.Windows.Forms.TextBox();
            this.geneName = new System.Windows.Forms.TextBox();
            this.deleteGene = new System.Windows.Forms.Button();
            this.childrenBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.speciesSelector = new System.Windows.Forms.ComboBox();
            this.geneSelector = new System.Windows.Forms.ComboBox();
            this.addGene = new System.Windows.Forms.Button();
            this.removeGene = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.notes = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.allGeneBox = new System.Windows.Forms.ListBox();
            this.weightTimelineBox = new System.Windows.Forms.ListBox();
            this.addWeightButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.weightAdder = new System.Windows.Forms.NumericUpDown();
            this.removeWeightButton = new System.Windows.Forms.Button();
            this.ateCounter = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.shedCounter = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.overviewSpeciesSelector = new System.Windows.Forms.ComboBox();
            this.overviewTextboxMale = new System.Windows.Forms.TextBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.overviewDockFemale = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.snakesTab.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weightAdder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ateCounter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shedCounter)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.financeTab);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.snakesTab);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1149, 480);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // financeTab
            // 
            this.financeTab.Location = new System.Drawing.Point(4, 22);
            this.financeTab.Name = "financeTab";
            this.financeTab.Padding = new System.Windows.Forms.Padding(3);
            this.financeTab.Size = new System.Drawing.Size(1141, 454);
            this.financeTab.TabIndex = 1;
            this.financeTab.Text = "Centjes";
            this.financeTab.UseVisualStyleBackColor = true;
            // 
            // snakesTab
            // 
            this.snakesTab.Controls.Add(this.button1);
            this.snakesTab.Controls.Add(this.shedCounter);
            this.snakesTab.Controls.Add(this.label13);
            this.snakesTab.Controls.Add(this.label12);
            this.snakesTab.Controls.Add(this.ateCounter);
            this.snakesTab.Controls.Add(this.removeWeightButton);
            this.snakesTab.Controls.Add(this.weightAdder);
            this.snakesTab.Controls.Add(this.label11);
            this.snakesTab.Controls.Add(this.addWeightButton);
            this.snakesTab.Controls.Add(this.weightTimelineBox);
            this.snakesTab.Controls.Add(this.allGeneBox);
            this.snakesTab.Controls.Add(this.label10);
            this.snakesTab.Controls.Add(this.notes);
            this.snakesTab.Controls.Add(this.label9);
            this.snakesTab.Controls.Add(this.label8);
            this.snakesTab.Controls.Add(this.removeGene);
            this.snakesTab.Controls.Add(this.addGene);
            this.snakesTab.Controls.Add(this.geneSelector);
            this.snakesTab.Controls.Add(this.speciesSelector);
            this.snakesTab.Controls.Add(this.label7);
            this.snakesTab.Controls.Add(this.childrenBox);
            this.snakesTab.Controls.Add(this.genderBox);
            this.snakesTab.Controls.Add(this.label4);
            this.snakesTab.Controls.Add(this.motherSelector);
            this.snakesTab.Controls.Add(this.fatherSelector);
            this.snakesTab.Controls.Add(this.label3);
            this.snakesTab.Controls.Add(this.label2);
            this.snakesTab.Controls.Add(this.dateTimePicker1);
            this.snakesTab.Controls.Add(this.selectedSnakeName);
            this.snakesTab.Controls.Add(this.label1);
            this.snakesTab.Controls.Add(this.panel1);
            this.snakesTab.Location = new System.Drawing.Point(4, 22);
            this.snakesTab.Name = "snakesTab";
            this.snakesTab.Size = new System.Drawing.Size(1141, 454);
            this.snakesTab.TabIndex = 2;
            this.snakesTab.Text = "Slangen";
            this.snakesTab.UseVisualStyleBackColor = true;
            // 
            // selectedSnakeName
            // 
            this.selectedSnakeName.Location = new System.Drawing.Point(55, 28);
            this.selectedSnakeName.Name = "selectedSnakeName";
            this.selectedSnakeName.Size = new System.Drawing.Size(277, 20);
            this.selectedSnakeName.TabIndex = 4;
            this.selectedSnakeName.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Name: ";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.snakeSelector);
            this.panel1.Controls.Add(this.newSnakeButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1141, 22);
            this.panel1.TabIndex = 2;
            // 
            // snakeSelector
            // 
            this.snakeSelector.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.snakeSelector.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.snakeSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.snakeSelector.FormattingEnabled = true;
            this.snakeSelector.Location = new System.Drawing.Point(0, 0);
            this.snakeSelector.Name = "snakeSelector";
            this.snakeSelector.Size = new System.Drawing.Size(1040, 21);
            this.snakeSelector.TabIndex = 0;
            this.snakeSelector.SelectedIndexChanged += new System.EventHandler(this.snakeSelector_SelectedIndexChanged);
            // 
            // newSnakeButton
            // 
            this.newSnakeButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.newSnakeButton.Location = new System.Drawing.Point(1040, 0);
            this.newSnakeButton.Name = "newSnakeButton";
            this.newSnakeButton.Size = new System.Drawing.Size(101, 22);
            this.newSnakeButton.TabIndex = 1;
            this.newSnakeButton.Text = "Nieuwe Slang";
            this.newSnakeButton.UseVisualStyleBackColor = true;
            this.newSnakeButton.Click += new System.EventHandler(this.newSnakeButton_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(424, 28);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(293, 20);
            this.dateTimePicker1.TabIndex = 5;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(335, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Geboortedatum:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Vader:";
            // 
            // fatherSelector
            // 
            this.fatherSelector.FormattingEnabled = true;
            this.fatherSelector.Location = new System.Drawing.Point(55, 55);
            this.fatherSelector.Name = "fatherSelector";
            this.fatherSelector.Size = new System.Drawing.Size(277, 21);
            this.fatherSelector.TabIndex = 8;
            this.fatherSelector.SelectedIndexChanged += new System.EventHandler(this.fatherSelector_SelectedIndexChanged);
            // 
            // motherSelector
            // 
            this.motherSelector.FormattingEnabled = true;
            this.motherSelector.Location = new System.Drawing.Point(424, 54);
            this.motherSelector.Name = "motherSelector";
            this.motherSelector.Size = new System.Drawing.Size(293, 21);
            this.motherSelector.TabIndex = 9;
            this.motherSelector.SelectedIndexChanged += new System.EventHandler(this.motherSelector_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(372, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Moeder:";
            // 
            // genderBox
            // 
            this.genderBox.FormattingEnabled = true;
            this.genderBox.Location = new System.Drawing.Point(723, 28);
            this.genderBox.Name = "genderBox";
            this.genderBox.Size = new System.Drawing.Size(103, 21);
            this.genderBox.TabIndex = 11;
            this.genderBox.SelectedIndexChanged += new System.EventHandler(this.genderBox_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1141, 454);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Soorten";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.speciesDescription);
            this.splitContainer1.Panel1.Controls.Add(this.speciesName);
            this.splitContainer1.Panel1.Controls.Add(this.deleteSpecies);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.geneDescription);
            this.splitContainer1.Panel2.Controls.Add(this.geneName);
            this.splitContainer1.Panel2.Controls.Add(this.deleteGene);
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Size = new System.Drawing.Size(1141, 454);
            this.splitContainer1.SplitterDistance = 569;
            this.splitContainer1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.newSpecies);
            this.panel2.Controls.Add(this.speciesBox);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(569, 53);
            this.panel2.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Soorten";
            // 
            // speciesBox
            // 
            this.speciesBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.speciesBox.FormattingEnabled = true;
            this.speciesBox.Location = new System.Drawing.Point(0, 32);
            this.speciesBox.Name = "speciesBox";
            this.speciesBox.Size = new System.Drawing.Size(569, 21);
            this.speciesBox.TabIndex = 1;
            this.speciesBox.SelectedIndexChanged += new System.EventHandler(this.speciesBox_SelectedIndexChanged);
            // 
            // newSpecies
            // 
            this.newSpecies.Dock = System.Windows.Forms.DockStyle.Right;
            this.newSpecies.Location = new System.Drawing.Point(464, 13);
            this.newSpecies.Name = "newSpecies";
            this.newSpecies.Size = new System.Drawing.Size(105, 19);
            this.newSpecies.TabIndex = 2;
            this.newSpecies.Text = "Nieuwe Soort";
            this.newSpecies.UseVisualStyleBackColor = true;
            this.newSpecies.Click += new System.EventHandler(this.newSpecies_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.NewGene);
            this.panel3.Controls.Add(this.geneBox);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(568, 53);
            this.panel3.TabIndex = 1;
            // 
            // NewGene
            // 
            this.NewGene.Dock = System.Windows.Forms.DockStyle.Right;
            this.NewGene.Location = new System.Drawing.Point(465, 13);
            this.NewGene.Name = "NewGene";
            this.NewGene.Size = new System.Drawing.Size(103, 19);
            this.NewGene.TabIndex = 2;
            this.NewGene.Text = "Nieuw Gen";
            this.NewGene.UseVisualStyleBackColor = true;
            this.NewGene.Click += new System.EventHandler(this.NewGene_Click);
            // 
            // geneBox
            // 
            this.geneBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.geneBox.FormattingEnabled = true;
            this.geneBox.Location = new System.Drawing.Point(0, 32);
            this.geneBox.Name = "geneBox";
            this.geneBox.Size = new System.Drawing.Size(568, 21);
            this.geneBox.TabIndex = 1;
            this.geneBox.SelectedIndexChanged += new System.EventHandler(this.geneBox_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Genetica";
            // 
            // deleteSpecies
            // 
            this.deleteSpecies.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.deleteSpecies.Location = new System.Drawing.Point(0, 431);
            this.deleteSpecies.Name = "deleteSpecies";
            this.deleteSpecies.Size = new System.Drawing.Size(569, 23);
            this.deleteSpecies.TabIndex = 1;
            this.deleteSpecies.Text = "Verwijderen";
            this.deleteSpecies.UseVisualStyleBackColor = true;
            this.deleteSpecies.Click += new System.EventHandler(this.deleteSpecies_Click);
            // 
            // speciesName
            // 
            this.speciesName.Dock = System.Windows.Forms.DockStyle.Top;
            this.speciesName.Location = new System.Drawing.Point(0, 53);
            this.speciesName.Name = "speciesName";
            this.speciesName.Size = new System.Drawing.Size(569, 20);
            this.speciesName.TabIndex = 2;
            this.speciesName.TextChanged += new System.EventHandler(this.speciesName_TextChanged);
            // 
            // speciesDescription
            // 
            this.speciesDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.speciesDescription.Location = new System.Drawing.Point(0, 73);
            this.speciesDescription.Multiline = true;
            this.speciesDescription.Name = "speciesDescription";
            this.speciesDescription.Size = new System.Drawing.Size(569, 358);
            this.speciesDescription.TabIndex = 3;
            this.speciesDescription.TextChanged += new System.EventHandler(this.speciesDescription_TextChanged);
            // 
            // geneDescription
            // 
            this.geneDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.geneDescription.Location = new System.Drawing.Point(0, 73);
            this.geneDescription.Multiline = true;
            this.geneDescription.Name = "geneDescription";
            this.geneDescription.Size = new System.Drawing.Size(568, 358);
            this.geneDescription.TabIndex = 6;
            this.geneDescription.TextChanged += new System.EventHandler(this.geneDescription_TextChanged);
            // 
            // geneName
            // 
            this.geneName.Dock = System.Windows.Forms.DockStyle.Top;
            this.geneName.Location = new System.Drawing.Point(0, 53);
            this.geneName.Name = "geneName";
            this.geneName.Size = new System.Drawing.Size(568, 20);
            this.geneName.TabIndex = 5;
            this.geneName.TextChanged += new System.EventHandler(this.geneName_TextChanged);
            // 
            // deleteGene
            // 
            this.deleteGene.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.deleteGene.Location = new System.Drawing.Point(0, 431);
            this.deleteGene.Name = "deleteGene";
            this.deleteGene.Size = new System.Drawing.Size(568, 23);
            this.deleteGene.TabIndex = 4;
            this.deleteGene.Text = "Verwijderen";
            this.deleteGene.UseVisualStyleBackColor = true;
            this.deleteGene.Click += new System.EventHandler(this.deleteGene_Click);
            // 
            // childrenBox
            // 
            this.childrenBox.Location = new System.Drawing.Point(541, 113);
            this.childrenBox.Multiline = true;
            this.childrenBox.Name = "childrenBox";
            this.childrenBox.ReadOnly = true;
            this.childrenBox.Size = new System.Drawing.Size(285, 303);
            this.childrenBox.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(538, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Kinderen";
            // 
            // speciesSelector
            // 
            this.speciesSelector.FormattingEnabled = true;
            this.speciesSelector.Location = new System.Drawing.Point(55, 113);
            this.speciesSelector.Name = "speciesSelector";
            this.speciesSelector.Size = new System.Drawing.Size(277, 21);
            this.speciesSelector.TabIndex = 14;
            this.speciesSelector.SelectedIndexChanged += new System.EventHandler(this.speciesSelector_SelectedIndexChanged);
            // 
            // geneSelector
            // 
            this.geneSelector.FormattingEnabled = true;
            this.geneSelector.Location = new System.Drawing.Point(55, 140);
            this.geneSelector.Name = "geneSelector";
            this.geneSelector.Size = new System.Drawing.Size(211, 21);
            this.geneSelector.TabIndex = 15;
            this.geneSelector.SelectedIndexChanged += new System.EventHandler(this.geneSelector_SelectedIndexChanged);
            // 
            // addGene
            // 
            this.addGene.Location = new System.Drawing.Point(272, 138);
            this.addGene.Name = "addGene";
            this.addGene.Size = new System.Drawing.Size(27, 23);
            this.addGene.TabIndex = 17;
            this.addGene.Text = "+";
            this.addGene.UseVisualStyleBackColor = true;
            this.addGene.Click += new System.EventHandler(this.addGene_Click);
            // 
            // removeGene
            // 
            this.removeGene.Location = new System.Drawing.Point(305, 138);
            this.removeGene.Name = "removeGene";
            this.removeGene.Size = new System.Drawing.Size(27, 23);
            this.removeGene.TabIndex = 18;
            this.removeGene.Text = "-";
            this.removeGene.UseVisualStyleBackColor = true;
            this.removeGene.Click += new System.EventHandler(this.removeGene_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 116);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Soort:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Genen:";
            // 
            // notes
            // 
            this.notes.Location = new System.Drawing.Point(338, 113);
            this.notes.Multiline = true;
            this.notes.Name = "notes";
            this.notes.Size = new System.Drawing.Size(194, 303);
            this.notes.TabIndex = 21;
            this.notes.TextChanged += new System.EventHandler(this.notes_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(335, 97);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Notities";
            // 
            // allGeneBox
            // 
            this.allGeneBox.FormattingEnabled = true;
            this.allGeneBox.Location = new System.Drawing.Point(8, 165);
            this.allGeneBox.Name = "allGeneBox";
            this.allGeneBox.Size = new System.Drawing.Size(324, 251);
            this.allGeneBox.TabIndex = 23;
            this.allGeneBox.SelectedIndexChanged += new System.EventHandler(this.allGeneBox_SelectedIndexChanged);
            // 
            // weightTimelineBox
            // 
            this.weightTimelineBox.FormattingEnabled = true;
            this.weightTimelineBox.Location = new System.Drawing.Point(832, 61);
            this.weightTimelineBox.Name = "weightTimelineBox";
            this.weightTimelineBox.Size = new System.Drawing.Size(301, 355);
            this.weightTimelineBox.TabIndex = 24;
            this.weightTimelineBox.SelectedIndexChanged += new System.EventHandler(this.weightTimelineBox_SelectedIndexChanged);
            // 
            // addWeightButton
            // 
            this.addWeightButton.Location = new System.Drawing.Point(1066, 26);
            this.addWeightButton.Name = "addWeightButton";
            this.addWeightButton.Size = new System.Drawing.Size(72, 23);
            this.addWeightButton.TabIndex = 26;
            this.addWeightButton.Text = "+Gewicht";
            this.addWeightButton.UseVisualStyleBackColor = true;
            this.addWeightButton.Click += new System.EventHandler(this.addWeightButton_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1037, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "gram";
            // 
            // weightAdder
            // 
            this.weightAdder.DecimalPlaces = 2;
            this.weightAdder.Location = new System.Drawing.Point(832, 28);
            this.weightAdder.Maximum = new decimal(new int[] {
            276447232,
            23283,
            0,
            0});
            this.weightAdder.Name = "weightAdder";
            this.weightAdder.Size = new System.Drawing.Size(206, 20);
            this.weightAdder.TabIndex = 28;
            this.weightAdder.ThousandsSeparator = true;
            // 
            // removeWeightButton
            // 
            this.removeWeightButton.Location = new System.Drawing.Point(1019, 419);
            this.removeWeightButton.Name = "removeWeightButton";
            this.removeWeightButton.Size = new System.Drawing.Size(114, 23);
            this.removeWeightButton.TabIndex = 29;
            this.removeWeightButton.Text = "Gewicht Verwijderen";
            this.removeWeightButton.UseVisualStyleBackColor = true;
            this.removeWeightButton.Click += new System.EventHandler(this.removeWeightButton_Click);
            // 
            // ateCounter
            // 
            this.ateCounter.Location = new System.Drawing.Point(91, 422);
            this.ateCounter.Maximum = new decimal(new int[] {
            276447232,
            23283,
            0,
            0});
            this.ateCounter.Name = "ateCounter";
            this.ateCounter.Size = new System.Drawing.Size(241, 20);
            this.ateCounter.TabIndex = 30;
            this.ateCounter.ValueChanged += new System.EventHandler(this.ateCounter_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 424);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "Keren gegeten";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(341, 424);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Keren Verveld";
            // 
            // shedCounter
            // 
            this.shedCounter.Location = new System.Drawing.Point(421, 422);
            this.shedCounter.Maximum = new decimal(new int[] {
            276447232,
            23283,
            0,
            0});
            this.shedCounter.Name = "shedCounter";
            this.shedCounter.Size = new System.Drawing.Size(241, 20);
            this.shedCounter.TabIndex = 33;
            this.shedCounter.ValueChanged += new System.EventHandler(this.shedCounter_ValueChanged);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Location = new System.Drawing.Point(798, 419);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(132, 23);
            this.button1.TabIndex = 34;
            this.button1.Text = "Verwijder Slang";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer2);
            this.tabPage2.Controls.Add(this.overviewSpeciesSelector);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1141, 454);
            this.tabPage2.TabIndex = 4;
            this.tabPage2.Text = "Overzicht";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // overviewSpeciesSelector
            // 
            this.overviewSpeciesSelector.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.overviewSpeciesSelector.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.overviewSpeciesSelector.Dock = System.Windows.Forms.DockStyle.Top;
            this.overviewSpeciesSelector.FormattingEnabled = true;
            this.overviewSpeciesSelector.Location = new System.Drawing.Point(0, 0);
            this.overviewSpeciesSelector.Name = "overviewSpeciesSelector";
            this.overviewSpeciesSelector.Size = new System.Drawing.Size(1141, 21);
            this.overviewSpeciesSelector.TabIndex = 0;
            this.overviewSpeciesSelector.SelectedIndexChanged += new System.EventHandler(this.overviewSpeciesSelector_SelectedIndexChanged);
            // 
            // overviewTextboxMale
            // 
            this.overviewTextboxMale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.overviewTextboxMale.Location = new System.Drawing.Point(0, 0);
            this.overviewTextboxMale.Multiline = true;
            this.overviewTextboxMale.Name = "overviewTextboxMale";
            this.overviewTextboxMale.ReadOnly = true;
            this.overviewTextboxMale.Size = new System.Drawing.Size(559, 433);
            this.overviewTextboxMale.TabIndex = 1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 21);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.overviewTextboxMale);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.overviewDockFemale);
            this.splitContainer2.Size = new System.Drawing.Size(1141, 433);
            this.splitContainer2.SplitterDistance = 559;
            this.splitContainer2.TabIndex = 2;
            // 
            // overviewDockFemale
            // 
            this.overviewDockFemale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.overviewDockFemale.Location = new System.Drawing.Point(0, 0);
            this.overviewDockFemale.Multiline = true;
            this.overviewDockFemale.Name = "overviewDockFemale";
            this.overviewDockFemale.ReadOnly = true;
            this.overviewDockFemale.Size = new System.Drawing.Size(578, 433);
            this.overviewDockFemale.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1149, 480);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Slangies";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.snakesTab.ResumeLayout(false);
            this.snakesTab.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weightAdder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ateCounter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shedCounter)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage financeTab;
        private System.Windows.Forms.TabPage snakesTab;
        private System.Windows.Forms.Button newSnakeButton;
        private System.Windows.Forms.ComboBox snakeSelector;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox selectedSnakeName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox motherSelector;
        private System.Windows.Forms.ComboBox fatherSelector;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox genderBox;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button newSpecies;
        private System.Windows.Forms.ComboBox speciesBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button NewGene;
        private System.Windows.Forms.ComboBox geneBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox speciesDescription;
        private System.Windows.Forms.TextBox speciesName;
        private System.Windows.Forms.Button deleteSpecies;
        private System.Windows.Forms.TextBox geneDescription;
        private System.Windows.Forms.TextBox geneName;
        private System.Windows.Forms.Button deleteGene;
        private System.Windows.Forms.TextBox childrenBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox notes;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button removeGene;
        private System.Windows.Forms.Button addGene;
        private System.Windows.Forms.ComboBox geneSelector;
        private System.Windows.Forms.ComboBox speciesSelector;
        private System.Windows.Forms.Button removeWeightButton;
        private System.Windows.Forms.NumericUpDown weightAdder;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button addWeightButton;
        private System.Windows.Forms.ListBox weightTimelineBox;
        private System.Windows.Forms.ListBox allGeneBox;
        private System.Windows.Forms.NumericUpDown shedCounter;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown ateCounter;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TextBox overviewTextboxMale;
        private System.Windows.Forms.TextBox overviewDockFemale;
        private System.Windows.Forms.ComboBox overviewSpeciesSelector;
    }
}

