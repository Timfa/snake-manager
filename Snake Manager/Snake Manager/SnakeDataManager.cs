﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake_Manager
{
    public static class SnakeDataManager
    {
        public static string DefaultPath
        {
            get
            {
                return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "snakedata.json");
            }
        }

        public const int SaveVersion = 1;
        public class SnakeData
        {
            public int Version = SnakeDataManager.SaveVersion;
            public int LastIndex;
            public List<Snake> AllSnakes;
            public List<Snake.SnakeGene> AllGenes;
            public List<Snake.SnakeSpecies> AllSpecies;
        }

        public static void SaveData(string location)
        {
            SnakeData data = new SnakeData();
            data.AllSnakes = Snake.AllSnakes;
            data.AllGenes = Snake.AllGenes;
            data.AllSpecies = Snake.AllSpecies;
            data.LastIndex = Snake.LastIndex;

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            File.WriteAllText(location, json);
        }

        public static SnakeData LoadData(string location)
        {
            string json = File.ReadAllText(location);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<SnakeData>(json);
        }
    }
}
