﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake_Manager
{
    public class Snake
    {
        public static int LastIndex;
        public static List<SnakeGene> AllGenes = new List<SnakeGene>();
        public static List<SnakeSpecies> AllSpecies = new List<SnakeSpecies>();
        public static List<Snake> AllSnakes = new List<Snake>();

        public string Name = "New Snake";

        public string Notes = "";

        public Genders Gender;
        public int Index;
        public int MotherID = -1, FatherID = -1;
        public DateTime BirthDate;
        public List<SnakeWeight> WeightTimeline;

        public SnakeSpecies Species;
        public List<SnakeGene> Genes;

        public int TimesEaten, TimesShedded;

        public Snake()
        {
            Index = LastIndex + 1;
            LastIndex = Index;
            FatherID = -1;
            MotherID = -1;
            AllSnakes.Add(this);
        }

        ~Snake()
        {
            if(AllSnakes.Contains(this))
                AllSnakes.Remove(this);
        }

        public override string ToString()
        {
            return Name + ", " + AgeYears + " jaar oud " + Gender.ToString() + " " + Species.Name;
        }

        public SnakeWeight Weight
        {
            get
            {
                if (WeightTimeline == null || WeightTimeline.Count == 0)
                    return new SnakeWeight();

                return WeightTimeline.Last();
            }
        }

        public int AgeYears
        {
            get
            {
                return (int)Math.Floor(((DateTime.Now - BirthDate).TotalDays / 365));
            }
        }

        public struct SnakeWeight
        {
            public double WeightGrams;
            public DateTime DateWeighed;
        }

        public struct SnakeSpecies
        {
            public string Name;
            public string Description;
        }

        public struct SnakeGene
        {
            public string Name;
            public string Description;
        }

        public enum Genders
        {
            Mannelijk,
            Vrouwelijk
        }
    }
}
