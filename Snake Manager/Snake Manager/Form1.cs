﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake_Manager
{
    public partial class Form1 : Form
    {
        private Snake selectedSnake;
        private int selectedIndex;

        public Form1()
        {
            InitializeComponent();

            try
            {
                SnakeDataManager.SnakeData data = SnakeDataManager.LoadData(SnakeDataManager.DefaultPath);
                Snake.AllGenes = data.AllGenes;
                Snake.AllSpecies = data.AllSpecies;
                Snake.LastIndex = data.LastIndex;
            }
            catch(Exception e)
            {
                Console.WriteLine("No data found");
                Console.WriteLine(e.Message);
            }

            genderBox.Items.AddRange(Enum.GetNames(typeof(Snake.Genders)));

            RefreshList();
            snakeSelector.SelectedIndex = snakeSelector.Items.Count - 1;

            RefreshSpeciesList();
            RefreshAllGeneList();

            speciesBox.SelectedIndex = Snake.AllSpecies.Count - 1;
            geneBox.SelectedIndex = Snake.AllGenes.Count - 1;
            RefreshSpeciesOverviewSelector();
        }

        private void newSnakeButton_Click(object sender, EventArgs e)
        {
            Snake snake = new Snake();
            snake.Name = "New Snake";
            snake.BirthDate = DateTime.Now;

            Console.WriteLine("New snake added, currently: " + Snake.AllSnakes.Count);

            snakeSelector.Items.Add(snake);
            snakeSelector.SelectedIndex = snakeSelector.Items.Count - 1;
        }

        private void snakeSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedSnake = Snake.AllSnakes[snakeSelector.SelectedIndex];

            motherSelector.Items.Clear();
            fatherSelector.Items.Clear();

            motherSelector.Items.AddRange(Snake.AllSnakes.FindAll(o => o.Gender == Snake.Genders.Vrouwelijk).ToArray());
            fatherSelector.Items.AddRange(Snake.AllSnakes.FindAll(o => o.Gender == Snake.Genders.Mannelijk).ToArray());

            selectedSnakeName.Text = selectedSnake.Name;
            dateTimePicker1.Value = selectedSnake.BirthDate;
            genderBox.SelectedIndex = (int)selectedSnake.Gender;

            Console.WriteLine(selectedSnake.FatherID + " dadid");

            if (selectedSnake.FatherID >= 0)
                fatherSelector.SelectedItem = Snake.AllSnakes.Find(o => o.Index == selectedSnake.FatherID);
            else
            {
                fatherSelector.Text = "";
                fatherSelector.SelectionLength = 0;
            }

            if (selectedSnake.MotherID >= 0)
                motherSelector.SelectedItem = Snake.AllSnakes.Find(o => o.Index == selectedSnake.MotherID);
            else
            {
                motherSelector.Text = "";
                motherSelector.SelectionLength = 0;
            }

            List<Snake> children = Snake.AllSnakes.FindAll(o => o.FatherID == selectedSnake.Index || o.MotherID == selectedSnake.Index);

            childrenBox.Text = "";

            foreach(Snake snake in children)
            {
                childrenBox.Text += snake.ToString() + Environment.NewLine;
            }

            speciesSelector.Text = selectedSnake.Species.Name;
            RefreshGeneList();
            RefreshWeightTimeline();

            notes.Text = selectedSnake.Notes;
            shedCounter.Value = selectedSnake.TimesShedded;
            ateCounter.Value = selectedSnake.TimesEaten;
        }

        public void RefreshList()
        {
            int index = snakeSelector.SelectedIndex;
            snakeSelector.Items.Clear();
            snakeSelector.Items.AddRange(Snake.AllSnakes.ToArray());
            snakeSelector.SelectedIndex = index;
        }

        private void textBox1_TextChanged(object sender, EventArgs e) //name
        {
            selectedSnake.Name = selectedSnakeName.Text;
            RefreshList();

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            selectedSnake.BirthDate = dateTimePicker1.Value;
            RefreshList();

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void fatherSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedSnake.FatherID = ((Snake)fatherSelector.SelectedItem).Index;

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void motherSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedSnake.MotherID = ((Snake)motherSelector.SelectedItem).Index;

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void genderBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedSnake.Gender = (Snake.Genders)genderBox.SelectedIndex;
            RefreshList();

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void newSpecies_Click(object sender, EventArgs e)
        {
            Snake.SnakeSpecies newgene = new Snake.SnakeSpecies();
            newgene.Name = "New Species " + DateTime.Now.DayOfYear + DateTime.Now.Year + DateTime.Now.Minute;
            Snake.AllSpecies.Add(newgene);
            RefreshSpeciesList();

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void NewGene_Click(object sender, EventArgs e)
        {
            Snake.SnakeGene newgene = new Snake.SnakeGene();
            newgene.Name = "New Gene " + DateTime.Now.DayOfYear + DateTime.Now.Year + DateTime.Now.Minute;
            Snake.AllGenes.Add(newgene);
            RefreshAllGeneList();

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void speciesBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (speciesBox.SelectedIndex < 0)
                return;
            int index = speciesBox.SelectedIndex;
            speciesName.Text = Snake.AllSpecies[index].Name;
            speciesDescription.Text = Snake.AllSpecies[index].Description;
        }

        private void geneBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = geneBox.SelectedIndex;
            if (index < 0)
                return;

            geneName.Text = Snake.AllGenes[index].Name;
            geneDescription.Text = Snake.AllGenes[index].Description;
        }

        private void speciesName_TextChanged(object sender, EventArgs e)
        {
            int index = speciesBox.SelectedIndex;
            if (index < 0)
                return;

            Snake.SnakeSpecies species = Snake.AllSpecies[index];
            species.Name = speciesName.Text;
            Snake.AllSpecies[index] = species;

            speciesBox.Text = species.Name;

            RefreshSpeciesList();
            speciesBox.SelectedIndex = index;
            RefreshList();

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void geneName_TextChanged(object sender, EventArgs e)
        {
            int index = geneBox.SelectedIndex;
            if (index < 0)
                return;

            Snake.SnakeGene gene = Snake.AllGenes[index];
            gene.Name = geneName.Text;
            Snake.AllGenes[index] = gene;
            geneBox.Items[geneBox.SelectedIndex] = gene.Name;

            RefreshGeneList();
            RefreshGeneSelectorList();

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void speciesDescription_TextChanged(object sender, EventArgs e)
        {
            int index = speciesBox.SelectedIndex;
            if (index < 0)
                return;

            Snake.SnakeSpecies species = Snake.AllSpecies[index];
            species.Description = speciesDescription.Text;
            Snake.AllSpecies[index] = species;

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void geneDescription_TextChanged(object sender, EventArgs e)
        {
            Snake.SnakeGene gene = Snake.AllGenes[geneBox.SelectedIndex];
            gene.Description = geneDescription.Text;
            Snake.AllGenes[geneBox.SelectedIndex] = gene;

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void deleteSpecies_Click(object sender, EventArgs e)
        {
            foreach (Snake snake in Snake.AllSnakes)
            {
                if(snake.Species.Equals(Snake.AllSpecies[speciesBox.SelectedIndex]))
                {
                    MessageBox.Show("Er is nog een slang die deze soort geselecteerd heeft! (" + snake.Name + ")" + Environment.NewLine + "Verander deze voor de soort verwijderd kan worden.");
                    return;
                }
            }

            Snake.AllSpecies.RemoveAt(speciesBox.SelectedIndex);

            RefreshSpeciesList();

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void RefreshSpeciesList()
        {
            speciesBox.Items.Clear();
            string[] names = new string[Snake.AllSpecies.Count];

            for (int i = 0; i < names.Length; i++)
                names[i] = Snake.AllSpecies[i].Name;

            speciesBox.Items.AddRange(names);

            speciesSelector.Items.Clear();
            speciesSelector.Items.AddRange(names);
            RefreshSpeciesOverviewSelector();
        }

        private void RefreshAllGeneList()
        {
            geneBox.Items.Clear();
            string[] names = new string[Snake.AllGenes.Count];

            for (int i = 0; i < names.Length; i++)
                names[i] = Snake.AllGenes[i].Name;

            geneBox.Items.AddRange(names);

            RefreshGeneList();
        }

        private void deleteGene_Click(object sender, EventArgs e)
        {
            foreach(Snake snake in Snake.AllSnakes)
            {
                snake.Genes.RemoveAll(o => o.Name == Snake.AllGenes[geneBox.SelectedIndex].Name);
            }

            Snake.AllGenes.RemoveAt(geneBox.SelectedIndex);
            RefreshAllGeneList();

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void notes_TextChanged(object sender, EventArgs e)
        {
            selectedSnake.Notes = notes.Text;

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void geneSelector_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void addGene_Click(object sender, EventArgs e)
        {
            if (selectedSnake.Genes == null)
                selectedSnake.Genes = new List<Snake.SnakeGene>();

            foreach(Snake.SnakeGene gene in selectedSnake.Genes)
            {
                if (gene.Name == Snake.AllGenes[geneSelector.SelectedIndex].Name)
                    return;
            }

            selectedSnake.Genes.Add(Snake.AllGenes[geneSelector.SelectedIndex]);

            RefreshGeneList();

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void removeGene_Click(object sender, EventArgs e)
        {
            if (allGeneBox.SelectedItem != null)
                selectedSnake.Genes.RemoveAt(allGeneBox.SelectedIndex);

            RefreshGeneList();

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void RefreshGeneList()
        {
            allGeneBox.Items.Clear();

            if (selectedSnake == null)
                return;

            if (selectedSnake.Genes == null || selectedSnake.Genes.Count == 0)
                return;

            string[] names = new string[selectedSnake.Genes.Count];

            for (int i = 0; i < names.Length; i++)
                names[i] = selectedSnake.Genes[i].Name;

            allGeneBox.Items.AddRange(names);
        }

        private void RefreshGeneSelectorList()
        {
            geneSelector.Items.Clear();

            string[] names = new string[Snake.AllGenes.Count];

            for (int i = 0; i < names.Length; i++)
                names[i] = Snake.AllGenes[i].Name;

            geneSelector.Items.AddRange(names);
        }

        private void speciesSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = speciesSelector.SelectedIndex;
            if (index < 0)
                return;

            selectedSnake.Species = Snake.AllSpecies[index];
            RefreshList();

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void addWeightButton_Click(object sender, EventArgs e)
        {
            Snake.SnakeWeight weight = new Snake.SnakeWeight();
            weight.DateWeighed = DateTime.Now;
            weight.WeightGrams = (int)weightAdder.Value;

            if (selectedSnake.WeightTimeline == null)
                selectedSnake.WeightTimeline = new List<Snake.SnakeWeight>();

            selectedSnake.WeightTimeline.Add(weight);
            RefreshList();
            RefreshWeightTimeline();

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        public void RefreshWeightTimeline()
        {
            weightTimelineBox.Items.Clear();

            if (selectedSnake.WeightTimeline == null)
                selectedSnake.WeightTimeline = new List<Snake.SnakeWeight>();

            string[] lines = new string[selectedSnake.WeightTimeline.Count];

            for (int i = 0; i < lines.Length; i++)
                lines[i] = selectedSnake.WeightTimeline[i].DateWeighed.ToString("dd-MM-yyyy") + ": " + selectedSnake.WeightTimeline[i].WeightGrams + " grams";

            weightTimelineBox.Items.AddRange(lines);
        }

        private void weightTimelineBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void allGeneBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void removeWeightButton_Click(object sender, EventArgs e)
        {
            if(weightTimelineBox.SelectedIndex > 0)
                selectedSnake.WeightTimeline.RemoveAt(weightTimelineBox.SelectedIndex);

            RefreshWeightTimeline();

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void shedCounter_ValueChanged(object sender, EventArgs e)
        {
            selectedSnake.TimesShedded = (int)shedCounter.Value;

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void ateCounter_ValueChanged(object sender, EventArgs e)
        {
            selectedSnake.TimesEaten = (int)ateCounter.Value;

            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Dit kan niet ongedaan gemaakt worden! Weet je zeker dat je " + selectedSnake.Name + " permanent wilt verwijderen?", "Slang Verwijderen", MessageBoxButtons.YesNo);

            if(result == DialogResult.Yes)
            {
                Snake.AllSnakes.Remove(selectedSnake);
                snakeSelector.SelectedIndex = 0;
                RefreshList();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
            SnakeDataManager.SaveData(SnakeDataManager.DefaultPath);
            overviewSpeciesSelector.SelectedIndex = 0;
        }

        private void overviewSpeciesSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<Snake> specieSnakes = Snake.AllSnakes.FindAll(o => o.Species.Name == Snake.AllSpecies[overviewSpeciesSelector.SelectedIndex].Name);

            overviewTextboxMale.Text = "";
            overviewDockFemale.Text = "";

            foreach(Snake snake in specieSnakes)
            {
                string geneList = " [ ";

                if (snake.Genes != null)
                {
                    foreach (Snake.SnakeGene gene in snake.Genes)
                        geneList += gene.Name + ", ";
                }

                geneList.Substring(0, geneList.Length - 3);
                geneList += "]";

                if (snake.Gender == Snake.Genders.Mannelijk)
                    overviewTextboxMale.Text += snake.ToString() + geneList + Environment.NewLine;
                else
                    overviewDockFemale.Text += snake.ToString() + geneList + Environment.NewLine;
            }
        }

        public void RefreshSpeciesOverviewSelector()
        {
            overviewSpeciesSelector.Items.Clear();
            string[] names = new string[Snake.AllSpecies.Count];

            for (int i = 0; i < names.Length; i++)
                names[i] = Snake.AllSpecies[i].Name;

            overviewSpeciesSelector.Items.AddRange(names);
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshSpeciesList();
            overviewSpeciesSelector.SelectedIndex = 0;
        }
    }
}
